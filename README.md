# playbook-rhaap

Playbooks to setup Red Hat Ansible Automation Platform 2.0 and later.

## Documentation

More details can be found on the [Ansible Labs](https://www.ansible-labs.de) web site.

## Cloud provider environment variables

Make sure to provide your cloud provider access details as environment variables. For convenience, save it as a shell script which can be sourced when needed.

```bash
# Amazon AWS
export AWS_ACCESS_KEY_ID=''
export AWS_SECRET_ACCESS_KEY=''
# Azure
export AZURE_SECRET=
export AZURE_CLIENT_ID=
export AZURE_SUBSCRIPTION_ID=
export AZURE_TENANT=
# Google
export GCP_AUTH_KIND=serviceaccount
export GCP_SERVICE_ACCOUNT_FILE=~/.gcp.json
export GCP_PROJECT=myproject
# Automation Controller
export CONTROLLER_HOST=
export CONTROLLER_USERNAME=admin
export CONTROLLER_PASSWORD=
# RHV
export OVIRT_HOSTNAME=
export OVIRT_USERNAME=
export OVIRT_PASSWORD=
# VMware
export VMWARE_HOST=
export VMWARE_USER=
export VMWARE_PASSWORD=
```

it's only required to provide the credentials for your target provider. For example, if you plan to deploy on AWS, Azure and Google can be ignored and not defined.

## Variables

The minimum set of extra variables you have to define is documented in the [Setup extra variables](https://www.ansible-labs.de/installation/30-extra-variables/) section.

## Inventory

Since the `controller-instance` playbook is creating the cloud instance and adds it to the in-memory inventory, no inventory file is needed. You can use `/etc/ansible/hosts` if you have to provide an inventory file.

## Download Ansible Setup

Since Red Hat Ansible Automation Platform 2 can only be downloaded from [access.redhat.com](https://access.redhat.com) when you have a valid Subscription, you have to download the file manually and save it in the [roles/rhaap/files](roles/rhaap/files) folder. If you don't have a valid subscription, you can request a [free trial](https://www.redhat.com/en/technologies/management/ansible/try-it).

## Run the Playbook

It's recommended to use [Ansible Navigator](https://github.com/ansible/ansible-navigator/) with the [ee-ansible-ssa](https://gitlab.com/redhat-cop/ansible-ssa/ee-ansible-ssa) execution environment. Install Ansible navigator from the [Red Hat Repositories](https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/2.0-ea/html-single/ansible_navigator_creator_guide/index?lb_target=production) or by using pip (e.g. if you're on Fedora).

It's recommended to [generate a separate SSH key ring](https://www.ansible-labs.de/installation/10-setup-workstation/) for this project.

Start the Playbook:

```bash
# if you store your cloud provider credentials in a file, load them into your shell
source /path/to/your/environment-file
# make sure your ssh key is loaded to get access into your cloud instance (start ssh-agent first)
eval `ssh-agent -s`
ssh-add /path/to/key
# log into Quay.io to fetch the EE
podman login quay.io
# start the playbook
ansible-navigator run rhaap-install.yml -e @/path/to/main.yml --vault-password-file /path/to/.vault -e @/path/to/vault.yml -i /etc/ansible/hosts
```

## DNS Helper Playbook

Sometimes it's necessary to update or delete a DNS record manually. To simplify this task, there is a little helper playbook which uses the `ansible_ssa.general` collection to perform that task. Note that this playbook requires the `dns_private`, `dns_key` and `dns_suffix` variables to be set. It's probably most conveniently done, by just using the same variable file used to create the Red Hat Ansible Automation Platform Demo.

To create or update a DNS record:

```bash
ansible-navigator run dns-helper.yml -e @/path/to/your/variables --vault-password-file /path/to/.vault -e instance_name=test -e ipaddress=1.2.2.3
```

To delete a DNS record:

```bash
ansible-navigator run dns-helper.yml -e @/path/to/your/variables --vault-password-file /path/to/.vault -e instance_name=test -e remove=true
```

### Debug DNS issues

Perform the following tasks to debug DNS issues.

```bash
yum -y install bind-utils
```

Create a nsupdate file in `/tmp/nsupdate.txt`:

```raw
server ns.ansible-labs.de
zone <your_zone_name>.ansible-labs.de.
update add test.<your_zone_name>.ansible-labs.de. 600 A 1.2.3.4
send
```

Run `nsupdate` with your keys and the created file:

```bash
nsupdate -k /path/to/your/K<your_name>.key /tmp/nsupdate.txt
```

**NOTE:** The above command does not show an output message on success. For more details try:

```bash
nsupdate -L3 -D -k /path/to/your/K<your_name>.key /tmp/nsupdate.txt
```
